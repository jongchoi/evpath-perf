#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <stdint.h>
#include <locale.h>
#include <mpi.h>
#include <sys/time.h>

#include "evpath.h"
#include "evperf_cmdline.h"

#define DUMP(fmt, ...) fprintf(stdout, ">>> "fmt"\n", ## __VA_ARGS__)

// Global variables
int comm_rank, comm_size;

typedef struct simple_rec {
    int id;
    long len;
    char* data;
    double timestamp;
} simple_rec_t, *simple_rec_ptr_t;

static FMField simple_field_list[] =
{
    {"id", "integer", sizeof(int), FMOffset(simple_rec_ptr_t, id)},
    {"len", "integer", sizeof(long), FMOffset(simple_rec_ptr_t, len)},
    {"data", "char[len]", sizeof(char), FMOffset(simple_rec_ptr_t, data)},
    {"timestamp", "float", sizeof(double), FMOffset(simple_rec_ptr_t, timestamp)},
    {NULL, NULL, 0, 0}
};

static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec_t), NULL},
    {NULL, NULL}
};

static double arrsum(char* arr, long len)
{
    double sum = 0.0;
    long i;
    for (i = 0; i < len; i++)
    {
        sum += (double) arr[i];
    }

    return sum;
}

static void arrsetrnd(char* arr, long len)
{
    long i;
    for (i = 0; i < len; i++)
    {
        arr[i] = rand() % 128;
    }
}

/*
static long gettimeusec()
{
    struct timeval t;
    if (gettimeofday(&t, NULL)) {
        perror("gettimeofday");
        return -1;
    }
    return t.tv_sec * 1E6 + t.tv_usec;
}
*/

typedef struct stat_rec_ {
    double sum_deltat;
    int    n_deltat;
} stat_rec_t, *stat_rec_ptr_t;

static int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    double ltime = MPI_Wtime();
    simple_rec_ptr_t event = vevent;
    stat_rec_t* stat = (stat_rec_t*) client_data;

    if (event->len == 0)
    {
        double deltat = ltime - event->timestamp;
        DUMP("[%d] Delta time = %g", event->id, deltat);
        stat->n_deltat++;
        if (stat->n_deltat > 1) stat->sum_deltat += deltat; // skip the first deltat
    }
    else
    {
        double sum = arrsum(event->data, event->len);
        double deltat = ltime - event->timestamp;
        //DUMP("deltat, ltime, timestamp = %g, %g, %f, %f", deltat, ltime - event->timestamp, ltime, event->timestamp);
        if (stat->n_deltat > 2)
            deltat -= stat->sum_deltat/(double)(stat->n_deltat-1);

        DUMP("[%d] sum: %f, count: %d, Avg delta: %f, local - remote: %f",
             event->id,
             stat->sum_deltat,
             stat->n_deltat,
             stat->sum_deltat/(double)(stat->n_deltat-1),
             ltime - event->timestamp);
        DUMP("[%d] Received %'ld bytes, sum: %'.01f, elapsed: %.03f seconds, throughput: %'.03f KB/sec",
             event->id, event->len, sum, deltat, event->len/deltat/1024.0);

        long nbytes = event->len;
        double t_elap = deltat;
        MPI_Allreduce(MPI_IN_PLACE, &nbytes, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(MPI_IN_PLACE, &t_elap, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        t_elap = t_elap/comm_size; // average time
        
        if (comm_rank==0)
        {
            DUMP("[SUM] Received %'ld bytes, elapsed: %'.03f seconds, throughput: %'.03f KB/sec",
                 nbytes, t_elap, (double)nbytes/t_elap/1024.0);
        }
    }

    return 1;
}

/* this file is evpath/examples/net_send.c */
int main(int argc, char **argv)
{
    struct gengetopt_args_info args_info;

    if (evperf_cmdline_parser (argc, argv, &args_info) != 0)
        exit(1) ;

    long len = args_info.len_arg;
    int ncnt = args_info.num_arg;
    int interval = args_info.interval_arg;
    char *method = "ib";
    int port = 59900;
    int send_probe = args_info.probe_flag;

    typedef enum {SERVER, CLIENT} mode_t;
    mode_t mode = SERVER;

    if (args_info.client_flag)
        mode = CLIENT;

    if (args_info.len_given)
        len = args_info.len_arg;

    if (args_info.num_given)
        ncnt = args_info.num_arg;

    if (args_info.interval_given)
        interval = args_info.interval_arg;

    if (args_info.method_given)
        method = args_info.method_arg;

    if (args_info.port_given)
        port = args_info.port_arg;

    long len_min = len;
    if (args_info.all_flag)
        if (args_info.min_given)
            len_min = args_info.min_arg;

    if (len_min < 2)
    {
        printf("Min length is 2\n");
        exit (1);
    }
    
    int host_argc = argc - optind;
    char** host_argv = &argv[optind];

    char *contact_string;
    if (args_info.contact_given)
        contact_string = args_info.contact_arg;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

    if (!args_info.nooffset_flag) port += comm_rank;
    
    setlocale(LC_NUMERIC, "en_US.UTF-8");

    CManager cm;
    simple_rec_t msg;
    EVstone stone;
    EVsource source;
    char string_list[2048];
    attr_list contact_list;
    EVstone remote_stone;

    char *t = (char *) malloc(len * sizeof(char));
    assert(t != NULL);

    remote_stone = 0;
    contact_list = create_attr_list();

    cm = CManager_create();

    // Client
    if (mode == CLIENT)
    {
        if (args_info.contact_given)
        {
            if (sscanf(contact_string, "%d:%s", &remote_stone, &string_list[0]) != 2) {
                printf("Bad arguments \"%s\"\n", contact_string);
                exit(1);
            }
            contact_list = attr_list_from_string(string_list);
        }
        else if (!strcasecmp(method, "TCP"))
        {
            add_string_attr(contact_list,
                            attr_atom_from_string("CM_TRANSPORT"), strdup(method));

            add_string_attr(contact_list,
                            attr_atom_from_string("IP_HOST"), host_argv[comm_rank % host_argc]);
            add_int_attr(contact_list,
                         attr_atom_from_string("IP_PORT"), port);
        }
        else if (!strcasecmp(method, "IB"))
        {
            add_string_attr(contact_list,
                            attr_atom_from_string("CM_TRANSPORT"), strdup(method));

            add_string_attr(contact_list,
                            attr_atom_from_string("IP_HOST"), host_argv[comm_rank % host_argc]);
            add_int_attr(contact_list,
                         attr_atom_from_string("IP_PORT"), port);
        }
        else if (!strcasecmp(method, "NNTI"))
        {
            add_string_attr(contact_list,
                            attr_atom_from_string("CM_TRANSPORT"), strdup(method));
            add_string_attr(contact_list,
                            attr_atom_from_string("CM_NNTI_TRANSPORT"),
                            "ib");
            add_string_attr(contact_list,
                            attr_atom_from_string("IP_HOST"), host_argv[comm_rank % host_argc]);
            add_int_attr(contact_list,
                         attr_atom_from_string("NNTI_PORT"), port);
        }
        else
        {
            printf("No support yet: %s\n", method);
            exit(0);
        }

        CMlisten(cm);

        stone = EValloc_stone(cm);

        if (EVassoc_bridge_action(cm, stone, contact_list, remote_stone) == -1)
        {
            fprintf(stderr, "No connection. Exit.\n");
            exit(1);
        }

        source = EVcreate_submit_handle(cm, stone, simple_format_list);

        long len_ = 0;

        DUMP("Method: %s", method);
        for (len_ = len_min; len_ <= len; len_ *= 2)
        {
            int icnt = 0;
            long sum_n = 0;
            double sum_t = 0.0;

            if (send_probe)
                len_ = 0;

            for (icnt=0; icnt < ncnt; icnt++)
            {
                arrsetrnd(t, len_);
                double sum = arrsum(t, len_);

                long nbytes = 0;
                msg.id = icnt;
                msg.len = len_;
                msg.data = t;

                double t_elap;
                MPI_Barrier(MPI_COMM_WORLD);
                t_elap = MPI_Wtime();

                msg.timestamp = t_elap;
                EVsubmit(source, &msg, NULL);

                t_elap = MPI_Wtime() - t_elap;

                nbytes = len_ * sizeof(char);

                DUMP("[%d] Written %'ld bytes, sum: %'.01f, elapsed: %'.03f seconds, throughput: %'.03f KB/sec",
                     icnt, nbytes, sum, t_elap, (double)nbytes/t_elap/1024.0);
                     
                MPI_Allreduce(MPI_IN_PLACE, &nbytes, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
                MPI_Allreduce(MPI_IN_PLACE, &t_elap, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
                t_elap = t_elap/comm_size; // average time
                
                if (comm_rank==0)
                {
                    DUMP("[SUM] Written %'ld bytes, sum: %'.01f, elapsed: %'.03f seconds, throughput: %'.03f KB/sec",
                         nbytes, sum, t_elap, (double)nbytes/t_elap/1024.0);
                }

                sum_n += nbytes;
                sum_t += t_elap;


                CMsleep(cm, interval);
            }

            if (comm_rank==0)
            {
                DUMP("[AVG] Written %'ld bytes, elapsed %'.03f seconds, throughput %'.03f KB/sec",
                     sum_n/ncnt, sum_t/ncnt, (double)sum_n/sum_t/1024.0);
            }

            if (send_probe)
            {
                send_probe = 0;
                len_ = len_min/2;
            }


        }
    }
    else // Server
    {
        if (!strcasecmp(method, "TCP"))
        {
            add_int_attr(contact_list,
                         attr_atom_from_string("IP_PORT"), port);
        }
        else if (!strcasecmp(method, "IB"))
        {
            add_int_attr(contact_list,
                         attr_atom_from_string("IP_PORT"), port);
        }
        add_string_attr(contact_list,
                        attr_atom_from_string("CM_TRANSPORT"), strdup(method));
        CMlisten_specific(cm, contact_list);

        stone = EValloc_stone(cm);

        stat_rec_t stat;
        stat.sum_deltat = 0.0;
        stat.n_deltat = 0;

        EVassoc_terminal_action(cm, stone, simple_format_list, simple_handler, &stat);
        char *string_list_ = attr_list_to_string(CMget_contact_list(cm));
        printf("Contact list \"%d:%s\"\n", stone, string_list_);
        dump_attr_list(CMget_contact_list(cm));

        //CMrun_network(cm);
        CMfork_comm_thread(cm);

        while (1)
        {
            CMsleep(cm, 10);
        }
    }

    MPI_Finalize();
    return 0;
}
