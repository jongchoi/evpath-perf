MPICC = mpicc
MPICXX = mpicxx
CFLAGS = -g -Wall -gdwarf-3

EVPATH_INC = -I$(EVPATH_DIR)/include 
EVPATH_LIB = -L$(EVPATH_DIR)/lib -levpath -latl -lffs -ldill -lcercs_env -ldl -lpthread -lm

GENOPT=n

OBJS = evperf

ifeq ($(GENOPT), y)
OBJS := genopt $(OBJS)
endif

.PHONE: all genopt clean

all: $(OBJS)

evperf: evperf.c evperf_cmdline.c
	$(MPICC) $(CFLAGS) $(EVPATH_INC) -o $@ $^ $(EVPATH_LIB)

genopt: evperf_cmdline.ggo
	gengetopt --input=evperf_cmdline.ggo --func-name=evperf_cmdline_parser --file=evperf_cmdline

clean:
	rm -f *.o *.*~ evperf
